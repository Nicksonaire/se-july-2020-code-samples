import { Link, NavLink } from "react-router-dom";
const Nav = () => {
  return (
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          {/* TODO: talk about activeClassName */}
          <NavLink to="/counter">Counter</NavLink>
        </li>
        <li>
          <Link to="/people-form">People Form</Link>
        </li>
        <li>
          <Link to="/people">People List</Link>
        </li>
        <li>
          <Link to="/cats">Cats</Link>
        </li>
        <li>
          <Link to="/cats/harriot">Ben's Cat</Link>
        </li>
        <li>
          <Link to="/this-page-is-ether">Into the Ether</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
