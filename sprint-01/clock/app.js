const Instructions = () => <h1 id="instruction">You Know What Time It Is</h1>;

const App = () => (
  <React.Fragment>
    <Instructions />
    <Clock />
    <Clock />
    <Clock />
  </React.Fragment>
);

ReactDOM.render(<App />, document.getElementById("root"));

console.log("Download the React DevTools 👇🏿");
console.log("https://reactjs.org/blog/2015/09/02/new-react-developer-tools.html#installation");
