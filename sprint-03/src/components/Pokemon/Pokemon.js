import { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { PokemonItem } from "./PokemonItem";
import { PokemonList } from "./PokemonList";
import { pokemonApi } from "../../util";

export const Pokemon = () => {
  const [pokemon, setPokemon] = useState([]);
  const [error, setError] = useState(null);
  const match = useRouteMatch();

  useEffect(() => {
    async function fetchPokemon() {
      try {
        const result = await pokemonApi.getAll();
        setPokemon(result);
      } catch (err) {
        setError("Unable to fetch Pokemon");
      }
    }
    fetchPokemon();
  }, []);

  return (
    <>
      <Row>
        <Col>{error ? error : <PokemonList pokemon={pokemon} />}</Col>
        <Col>
          <Switch>
            <Route path={`${match.path}/:pokemonId`}>
              <PokemonItem />
            </Route>
            <Route path={match.path}>
              <h1>Please Select a pokemon</h1>
            </Route>
          </Switch>
        </Col>
      </Row>
    </>
  );
};
