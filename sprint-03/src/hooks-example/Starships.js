import { useState, useEffect } from "react";
import { getIdFromUrl } from "../util";

export const Starships = () => {
  const [starships, setStarships] = useState([]);

  useEffect(() => {
    // async function fetchStarships() {}
    const fetchStarships = async () => {
      try {
        const response = await fetch("https://swapi.dev/api/starships");
        const data = await response.json();
        setStarships(data.results);
      } catch (err) {
        console.error("Unable to fetch starships", err);
      }
    };

    fetchStarships();
  }, []);

  return (
    <>
      <ul>
        {starships.map((starship) => (
          <li key={getIdFromUrl(starship.url)}>{starship.name}</li>
        ))}
      </ul>
    </>
  );
};

// useEffect(() => {
//   fetch("https://swapi.dev/api/starships")
//     .then((response) => response.json())
//     .then((data) => {
//       setStarships(data.results);
//     })
//     .catch((err) => {
//       console.error(err);
//     });
// }, []);
