class App extends React.Component {
  state = {
    count: 0,
    text: "",
  };

  laughElement = new Audio("laugh.wav");
  clapsElement = new Audio("claps.wav");

  addFriend = () => {
    this.setState((state) => {
      const count = state.count + 1;
      const text = count < 5 ? "LAUGHING AT YOU!" : "CLAPPING FOR YOU!";
      return { count, text };
    });
  };

  checkFriends = () => {
    if (this.state.count < 5) {
      this.laughElement.play();
    } else {
      this.clapsElement.play();
    }
  };

  //   showMood = () => {
  //     const { count } = this.state;
  //     if (!count) {
  //       return null;
  //     }

  //     if (count < 5) {
  //       return "LAUGHING AT YOU!";
  //     }

  //     return "CLAPPING FOR YOU!";
  //   };

  render() {
    const { count, text } = this.state;
    return (
      <React.Fragment>
        <h1>Welcome to my Profile Page!</h1>
        <img src="./kenzie.png" />
        <p>Friends List: {this.state.count}</p>
        <button onClick={this.addFriend}>Add Friend!</button>
        <button onClick={this.checkFriends}>Check Friends</button>
        {/* <h1>{this.showMood()}</h1> */}
        <h1>{text}</h1>
      </React.Fragment>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
