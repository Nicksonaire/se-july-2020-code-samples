import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions as counterActions } from "../redux/actions/counter";

export const UseRedux = () => {
  const [num, setNum] = useState(0);
  // pull complexCounter from the redux store
  const complexCounter = useSelector((state) => state.complexCounter);
  // grab the dispatch function to dispatch actions
  const dispatch = useDispatch();
  // create a function to dispatch les actions
  const increment = () => dispatch(counterActions.increment());
  // create a function to dispatch the multiply action
  const multiply = () => dispatch(counterActions.multiply(num));

  return (
    <>
      <h1>Redux w/ hooks</h1>
      <p>Redux Number --> {complexCounter}</p>
      <p>State Number --> {num}</p>
      <input type="number" value={num} onChange={(e) => setNum(Number.parseInt(e.target.value, 10))} />
      <button onClick={increment}>Increment</button>
      <button
        onClick={() => {
          dispatch(counterActions.decrement());
        }}
      >
        Decrement
      </button>
      <button onClick={multiply}>Multiply By My Input</button>
    </>
  );
};
