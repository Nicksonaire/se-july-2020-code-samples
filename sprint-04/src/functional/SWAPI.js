/*

Loading State
State to hold our data


----
When the component Renders initially
  - fetch data
  - display data on the screen

-Map over all the data
  -display each individual item



*/

import { useReducer, useEffect } from "react";

// {
//   loading: Boolean,
//   people: [],
//   error: null,
// }
const initialState = {
  loading: false,
  people: [
    {
      name: "Edwina",
    },
  ],
  error: null,
};

function swapiReducer(state, { type, payload }) {
  switch (type) {
    case "FETCH_PEOPLE":
      return {
        ...state,
        loading: true,
      };
    case "FETCH_PEOPLE_SUCCESS":
      // state.people = payload; BADD DONT DO THIS EVER
      return {
        ...state,
        people: [...state.people, ...payload],
        loading: false,
        error: null,
      };
    case "FETCH_PEOPLE_FAILURE":
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
}

function fetchPeople() {
  console.log("dispatching fetchPeople");
  return { type: "FETCH_PEOPLE" };
}

function fetchPeopleSuccess(payload) {
  console.log("dispatching fetchPeopleSuccess", payload);
  return { type: "FETCH_PEOPLE_SUCCESS", payload };
}

function fetchPeopleFailure(payload) {
  console.log("dispatching fetchPeopleSuccess", payload);
  return { type: "FETCH_PEOPLE_FAILURE", payload };
}

export default function SwapiExample() {
  const [state, dispatch] = useReducer(swapiReducer, initialState);

  useEffect(() => {
    const fetchPeopleData = async () => {
      try {
        dispatch(fetchPeople());
        const response = await fetch("https://swapi.dev/api/people");
        const { results } = await response.json();
        dispatch(fetchPeopleSuccess(results));
      } catch (err) {
        console.error("Unable to fetch starships", err);
        dispatch(fetchPeopleFailure(err.message));
      }
    };

    fetchPeopleData();
  }, []);

  if (state.error) {
    return <h1>{state.error}</h1>;
  }
  if (state.loading) {
    return <h1>Loading bruh...</h1>;
  }
  return (
    <>
      <h1>In a galaxy far far away...</h1>
      <ul>
        {state.people.map((person) => (
          <li key={person.name}>{person.name}</li>
        ))}
      </ul>
    </>
  );
}
