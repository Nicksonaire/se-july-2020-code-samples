import { createStore, applyMiddleware } from "redux";
import reducer from "./reducers";

// TODO talk about middleware
const store = createStore(reducer /* applyMiddleware() */);

export default store;
